# Sainsbury's test exercise - scraper

This command line application can scrape Sainsbury's grocery site. Output is
pretty-formatted JSON in STDOUT. It makes using the scraper easy together with
other command-line utilities and easily testable as well.

In order to streamline performance, items are processed in parallel.

Running application:

	./scraper -url http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html

Default URL points to the test page.

## Preconditions

### For building executable

Go 1.7 or newer

Make sure that following dependencies are accessible for the project:

	go get github.com/yhat/scrape
	go get golang.org/x/net/html

Make sure that following commands are runnable:

	gofmt
	goimports
	go vet

### For testing

Unix (tested on Linux Ubuntu 16.04)

Ruby (tested with Ruby 2.3)

The test page must be accessible.

	go get golang.org/x/net/html

## Testing

For building and testing application, there is the file `build.sh` in the root
directory, which
* does formatting, import organising and diagnostics for the source code;
* builds executable;
* tests the application end-to-end

To do all of this, just run:

	./build.sh

