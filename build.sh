#!/bin/sh

# We take paths relative to this script.
BASEDIR=$(dirname "$0")
APP_MAIN=$BASEDIR/scraper.go
ANALYSER=$BASEDIR/test/analyse_output.rb
EXPECTATIONS_FILE=$BASEDIR/test/expectations.json

fail(){
  echo "" >&2
  echo "$1 failed, see reasons in the output above" >&2
  echo "Program will exit" >&2
  exit 1
}

echo "Diagnostics & format - START"

goimports -w $BASEDIR
goimports -w $BASEDIR
go vet $BASEDIR || fail "Go vet"

echo "Diagnostics & format - SUCCESS"

echo "Build - START"

# Build
go build $APP_MAIN || fail "Build"

echo "Build - SUCCESS - executable created: ./scraper"

# Test

echo "test - START"

./scraper | $ANALYSER $EXPECTATIONS_FILE || fail "Test"

echo "test - SUCCESS"

