// Test exercise for Sainsbury's
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math"
	"net/http"
	"strconv"
	"strings"
	"sync"

	"github.com/yhat/scrape"
	"golang.org/x/net/html"
)

// -- Structures for output - start

type Output struct {
	Results []Item  `json:"results"`
	Total   float64 `json:"total"`
}

type Item struct {
	Title       string  `json:"title"`
	Size        string  `json:"size"`
	UnitPrice   float64 `json:"unit_price"`
	Description string  `json:"description"`
}

// -- Structures for output - end

var (
	urlDefault = "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html"
	url        = flag.String("url", urlDefault, "URL of the main page to be scraped")
)

var items = make([]Item, 0)

func init() {
	flag.Parse()
}

// main scrapes Sainsbury's products page and outputs products' information as
// pretty-printed JSON into STDOUT.
func main() {
	processItems(getItemUrls())
}

func getItemUrls() []string {
	result := make([]string, 0)
	resp, err := http.Get(*url)
	defer resp.Body.Close()

	if err != nil {
		log.Fatalf("Could not get data from following URL: '%v'", *url)
	}

	root, err := html.Parse(resp.Body)
	if err != nil {
		log.Fatalf("Could not parse response body from following URL: '%v'", *url)
	}

	urlMatcher := func(n *html.Node) bool {
		if n.Parent != nil && n.Parent.Parent != nil && n.Type == html.ElementNode {
			return scrape.Attr(n.Parent.Parent, "class") == "productInfo"
		}
		return false
	}
	linkNodes := scrape.FindAll(root, urlMatcher)

	for _, eachLink := range linkNodes {
		result = append(result, scrape.Attr(eachLink, "href"))
	}
	return result
}

func processItems(itemUrls []string) {
	urlsLen := len(itemUrls)
	itemsCh := make(chan Item, urlsLen)

	var wg sync.WaitGroup
	wg.Add(urlsLen)

	for _, eachUrl := range itemUrls {
		go func(url string) {
			defer wg.Done()
			processItem(url, itemsCh)
		}(eachUrl)
	}

	wg.Wait()
	close(itemsCh)

	totalPrice := 0.0
	for eachItem := range itemsCh {
		items = append(items, eachItem)
		totalPrice += eachItem.UnitPrice
	}
	output := Output{items, toFixed(totalPrice, 2)}

	json, err := json.MarshalIndent(output, "", "\t")
	if err != nil {
		log.Fatalf("Could not parse output: '%v'", err)
	}
	fmt.Println(string(json))
}

// XXX: for fixing up some precision that gets lost!
func round(num float64) int {
	return int(num + math.Copysign(0.5, num))
}

func toFixed(num float64, precision int) float64 {
	output := math.Pow(10, float64(precision))
	return float64(round(num*output)) / output
}

func processItem(itemUrl string, itemsCh chan Item) {
	resp, err := http.Get(itemUrl)
	defer resp.Body.Close()

	if err != nil {
		log.Printf("Could not process item on URL: '%v', skipping it\n", *url)
		return
	}

	itemsCh <- parseItem(resp)
}

func parseItem(resp *http.Response) Item {
	result := Item{} // title, size, unit price, description
	result.Size = fmt.Sprintf("%.3fkB", float64(resp.ContentLength)/1000.0)

	root, err := html.Parse(resp.Body)
	if err != nil {
		return result // Just empty result on error
	}

	// -- Price ---
	priceMatcher := func(n *html.Node) bool {
		if n.Parent != nil && n.Type == html.TextNode {
			return scrape.Attr(n.Parent, "class") == "pricePerUnit"
		}
		return false
	}

	// We assume that first one found is correct
	priceNode, ok := scrape.Find(root, priceMatcher)
	if ok {
		price, err := strconv.ParseFloat(strings.Split(priceNode.Data, "£")[1], 64)
		if err == nil {
			result.UnitPrice = price
		}
	}

	// -- Title ---
	titleMatcher := func(n *html.Node) bool {
		if n.Parent != nil && n.Parent.Parent != nil && n.Type == html.TextNode {
			return scrape.Attr(n.Parent.Parent, "class") == "productTitleDescriptionContainer"
		}
		return false
	}
	titleNode, ok := scrape.Find(root, titleMatcher)
	if ok {
		result.Title = titleNode.Data
	}

	// -- Description ---
	descMatcher := func(n *html.Node) bool {
		if n.Parent != nil && n.Parent.Parent != nil && n.Type == html.TextNode {
			return scrape.Attr(n.Parent.Parent, "class") == "productText"
		}
		return false
	}
	descNode, ok := scrape.Find(root, descMatcher)
	if ok {
		result.Description = descNode.Data
	}

	return result
}
