#!/usr/bin/env ruby

# Test resutls analyser.
# Actual result is read from STDIN, expected results file is given as first argument.

require 'json'

def parse_json(raw, comment)
  JSON.parse(raw)
rescue JSON::ParserError
  STDERR.puts "No valid JSON for #{comment} result"
  STDERR.puts "Raw #{comment} result:"
  STDERR.puts "-----------------------"
  STDERR.puts raw
  STDERR.puts "-----------------------"
  STDERR.puts "Program will exit."
  exit 1
end

def get_expected
  expected_file = ARGV[0]
  unless expected_file && File.exist?(expected_file)
    STDERR.puts("Expectations file '#{expected_file}' does not exist.")
    STDERR.puts("Program will exit")
    exit 1
  end
  expected_raw = IO.read(expected_file)
  parse_json(expected_raw, "actual")
end

def get_actual
  actual_raw = $stdin.read
  parse_json(actual_raw, "actual")
end

def fail_test(comment)
  STDERR.puts("FAILED: #{comment}")
  exit 1
end

def check_attribute(expected, actual, attribute)
  return if expected[attribute] == actual[attribute]

  fail_test("Item '#{actual["title"]}', attr: '#{attribute}' - expected '#{expected[attribute]}', actual '#{actual[attribute]}'." )
end

def analyse_item(item, expected_results)
  expected_item = expected_results.select{|i| i["title"] == item["title"]}.first

  unless expected_item
    fail_test "Item '#{item}' not found among expected results"
  end

  check_attribute(expected_item, item, "unit_price")
  check_attribute(expected_item, item, "description")

  unless item["size"] =~ /\A\-\d+\.\d+kB\z/
    fail_test "Item '#{item}' has incorrectly formatted size."
  end
end

def analyse_results(expected, actual)
  unless expected["total"] == actual["total"]
    fail_test "Expected total of '#{expected["total"]}', but was '#{actual["total"]}'."
  end

  expected_results = expected["results"] || []
  actual_results = actual["results"] || []

  unless expected_results.size == actual_results.size
    fail_test "Expected #{expected_results.size} results, but got #{actual_results.size}"
  end

  actual_results.each do |each|
    analyse_item(each, expected_results)
  end

  puts "SUCCESS: all the tests ran successfully"
end

analyse_results(get_expected, get_actual)

